import { useState, createContext } from "react";

export const TaskContext = createContext();

export const TaskProvider = (props) => {
  const [tasks, setTasks] = useState([
    {
      id: 1,
      text: "Doctors Appointment",
      date: "23-06-2021",
      reminder: true,
    },
    {
      id: 2,
      text: "Meeting at office",
      date: "24-06-2021",
      reminder: true,
    },
    {
      id: 3,
      text: "Grocery shopping",
      date: "25-06-2021",
      reminder: false,
    },
  ]);

  // to render child components
  return (
    <TaskContext.Provider value={[tasks, setTasks]}>
      {props.children}
    </TaskContext.Provider>
  );
};
