import Header from "./components/Header";
import TaskList from "./components/TaskList";
import { TaskProvider } from "./contexts/TaskContext";

function App() {
  return (
    <TaskProvider>
      <div className="container">
        <Header />
        <TaskList />
      </div>
    </TaskProvider>
  );
}

export default App;
