import Button from "./Button";
import { useState, useEffect } from "react";
const AddTask = () => {
  const [isOpen, setIsOpen] = useState(false);
  const onClick = () => setIsOpen(true);

  useEffect(() => {
    document.title = isOpen ? `Edit mode` : `View mode`;
  });

  return (
    <>
      <Button color="green" text="Add" onClick={onClick} />
      {isOpen ? <AddTaskModal /> : null}
    </>
  );
};

const AddTaskModal = () => {
  return (
    <div className="modal">
      <form>
        <input type="text" name="Text" />
        <input type="date" name="Date" />
      </form>
    </div>
  );
};

export default AddTask;
