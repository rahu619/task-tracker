import { useContext, useState } from "react";
import { TaskContext } from "../contexts/TaskContext";
import Task from "./Task";

const TaskList = () => {
  const [tasks, setTasks] = useContext(TaskContext);
  return (
    <>
      {tasks.map((task) => (
        <Task key={task.id} text={task.text} />
      ))}
    </>
  );
};

export default TaskList;
